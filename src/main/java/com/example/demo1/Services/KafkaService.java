package com.example.demo1.Services;

import com.example.demo1.Deserializers.JSONDeserializer;
import com.example.demo1.Deserializers.JSONSerializer;
import com.example.demo1.Models.Temperature;
import com.example.demo1.RandomWordSource;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.streaming.siddhi.SiddhiCEP;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.producer.Producer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.stereotype.Service;

import java.util.Properties;

import static org.junit.Assert.assertEquals;

@Service
public class KafkaService {



    public static final KafkaTemplate<String, Temperature> kafkaTemplate = new KafkaTemplate<String, Temperature>(new ProducerFactory<String, Temperature>() {
        @Override
        public Producer<String, Temperature> createProducer() {
            return null;
        }
    });

   public void postInKafka(String topic, Temperature temperature){
       kafkaTemplate.send(topic, temperature);

   }


    public static FlinkKafkaConsumer011<String> createStringConsumerForTopic(
            String topic, String kafkaAddress, String kafkaGroup ) {

        Properties props = new Properties();
        props.setProperty("bootstrap.servers", kafkaAddress);
        props.setProperty("group.id",kafkaGroup);
        FlinkKafkaConsumer011<String> consumer = new FlinkKafkaConsumer011<>(
                topic, new SimpleStringSchema(), props);

        return consumer;
    }
    public static FlinkKafkaConsumer011<Temperature> createObjectConsumerForTopic(
            String topic, String kafkaAddress, String kafkaGroup ) {

        Properties props = new Properties();
        props.setProperty("bootstrap.servers", kafkaAddress);
        props.setProperty("group.id",kafkaGroup);
        FlinkKafkaConsumer011<Temperature> consumer = new FlinkKafkaConsumer011<Temperature>(
                topic, new JSONDeserializer<>(Temperature.class), props);

        return consumer;
    }

    public static FlinkKafkaProducer011<String> createStringProducer(
            String topic, String kafkaAddress){


        return new FlinkKafkaProducer011<>(kafkaAddress,
                topic, new SimpleStringSchema());
    }

    public static FlinkKafkaProducer011<Temperature> createObjectProducer(
            String topic, String kafkaAddress){


        return new FlinkKafkaProducer011<Temperature>(kafkaAddress,
                topic, new JSONSerializer());
    }

    public static class TupleToString implements MapFunction<Tuple1<Integer>,String> {
        @Override
        public String map(Tuple1<Integer>s) {
            return Integer.toString(s.f0);
        }
    }

    public static class StringToInteger implements MapFunction<String,Integer>{
        @Override
        public Integer map(String s){
            return Integer.parseInt(s);
        }
    }

    public void NotifyOnce() throws Exception {
        String inputTopic = "FlinkInput";
        String outputTopic = "FlinkOutput";
        String consumerGroup = "testgroup";
        String address = "localhost:9092";
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        SiddhiCEP cep = SiddhiCEP.getSiddhiEnvironment(environment);

        FlinkKafkaConsumer011<String> flinkKafkaConsumer = createStringConsumerForTopic(inputTopic, address, consumerGroup);

        DataStream<String> stringInputStream = environment.addSource(flinkKafkaConsumer);

        DataStream<Integer>integerInputStream = stringInputStream.map(new StringToInteger());

        FlinkKafkaProducer011<String> flinkKafkaProducer = createStringProducer(outputTopic, address);

        cep.registerStream("numberStream",integerInputStream,"numbers");

        DataStream<Tuple1<Integer>> output = cep
                .from("numberStream")
                .cql("from numberStream#window.length(3) select min(numbers) as minNumber having minNumber > 89 insert into  outputStream")
                .returns("outputStream");

        output
                .map(new TupleToString())
                .keyBy(s -> 1)
                .process(new KeyedProcessFunction<Integer, String, String>() {
                    private transient ValueState<Boolean> flagState;
                    private transient ValueState<Long> timerState;
                    private static final long ONE_MINUTES = 1000*60;

                    @Override
                    public void open(Configuration configuration){
                        ValueStateDescriptor<Boolean> flagDescriptor = new ValueStateDescriptor<Boolean>(
                                "flag", Types.BOOLEAN);
                        flagState = getRuntimeContext().getState(flagDescriptor);

                        ValueStateDescriptor<Long> timerDescriptor = new ValueStateDescriptor<>(
                                "timer-state",
                                Types.LONG);
                        timerState = getRuntimeContext().getState(timerDescriptor);

                    }

                    @Override
                    public void processElement(String s, Context context, Collector<String> collector) throws Exception {
                        Boolean lastNotification = flagState.value();
                        if(lastNotification == null){
                            collector.collect(s);
                            flagState.update(true);

                            long timer = context.timerService().currentProcessingTime() + ONE_MINUTES;
                            context.timerService().registerProcessingTimeTimer(timer);

                            timerState.update(timer);
                        }

                    }

                    @Override
                    public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) {
                        timerState.clear();
                        flagState.clear();
                    }

                })
                .addSink(flinkKafkaProducer);

        environment.execute();
    }

    public void deserializeTest() throws Exception{
        String inputTopic = "FlinkInputObject";
        String outputTopic = "FlinkOutputObject";
        String consumerGroup = "testgroup";
        String address = "localhost:9092";
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        FlinkKafkaConsumer011<Temperature> flinkKafkaConsumer = createObjectConsumerForTopic(inputTopic, address, consumerGroup);
        FlinkKafkaProducer011<Temperature> flinkKafkaProducer = createObjectProducer(outputTopic,address);

        DataStream<Temperature> temperatureDataStream = env.addSource(flinkKafkaConsumer);
        temperatureDataStream
                .addSink(flinkKafkaProducer);


    }


    public void testUnboundedPrimitiveTypeSourceAndReturnTuple() throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<String> input = env.addSource(new RandomWordSource(5).closeDelay(1500));
        SiddhiCEP cep = SiddhiCEP.getSiddhiEnvironment(env);
        cep.registerStream("wordStream",input,"words");
        DataStream<Tuple1<String>> output = cep
                .from("wordStream")
                .cql("from wordStream select words insert into  outputStream")
                .returns("outputStream");
        output.print();





    }

}
