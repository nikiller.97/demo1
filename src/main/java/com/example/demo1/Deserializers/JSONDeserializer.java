package com.example.demo1.Deserializers;

import com.esotericsoftware.minlog.Log;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JSONDeserializer<T> implements DeserializationSchema<T> {
    ObjectMapper objectMapper = new ObjectMapper();
    Class<T> classType;

    public JSONDeserializer(Class<T> classType){
        this.classType = classType;
    }
    @Override
    public T deserialize(byte[] bytes) {
        try {
            return objectMapper.readValue(bytes,classType);
        } catch (IOException e) {
            Log.error("JSONDeserializer : Invalid input ->" + new String(bytes));
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean isEndOfStream(T t) {
        return false;
    }

    @Override
    public TypeInformation getProducedType()
    {
        return TypeInformation.of(classType);
    }
}
