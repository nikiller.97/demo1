package com.example.demo1;

import com.example.demo1.Services.KafkaService;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Properties;

@SpringBootApplication
public class Demo1Application {
    public static void main(final String[] args) throws Exception {

        KafkaService kafkaService = new KafkaService();

        SpringApplication.run(Demo1Application.class, args);
        kafkaService.NotifyOnce();
    }

}
