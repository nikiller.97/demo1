package com.example.demo1.Controllers;

import com.example.demo1.Models.Temperature;
import com.example.demo1.Services.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class KafkaController {

    @Autowired
    KafkaService kafkaService;
    private static final String topic = "flink_input";

    @PostMapping(value = "/post")
    public String postInKafka(@RequestBody @Valid Temperature temperature){
        kafkaService.postInKafka(topic,temperature);
        return "temperature posted";

    }


}
